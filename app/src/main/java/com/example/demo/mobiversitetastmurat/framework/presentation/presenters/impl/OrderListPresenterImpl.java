package com.example.demo.mobiversitetastmurat.framework.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;

import com.example.demo.mobiversitetastmurat.R;
import com.example.demo.mobiversitetastmurat.domain.executor.Executor;
import com.example.demo.mobiversitetastmurat.domain.executor.MainThread;
import com.example.demo.mobiversitetastmurat.domain.interactors.GetNetworkCallInteractor;
import com.example.demo.mobiversitetastmurat.domain.interactors.impl.OrderListInteractorImpl;
import com.example.demo.mobiversitetastmurat.framework.network.data.impl.OrderListItemDataImpl;
import com.example.demo.mobiversitetastmurat.framework.presentation.model.response.OrderListResponseModel;
import com.example.demo.mobiversitetastmurat.framework.presentation.presenters.OrderListPresenter;
import com.example.demo.mobiversitetastmurat.framework.presentation.presenters.base.AbstractPresenter;
import com.example.demo.mobiversitetastmurat.framework.utils.Utilities;

/**
 * Created by Murat.
 */
public class OrderListPresenterImpl extends AbstractPresenter implements GetNetworkCallInteractor.Callback,
        OrderListPresenter {

    private OrderListPresenter.View view;
    private Context context;

    public OrderListPresenterImpl(Executor threadExecutor, MainThread mainThread, View view, Context context) {
        super(threadExecutor, mainThread);
        this.view = view;
        this.context = context;
    }

    @Override
    public void getOrderList() {
        view.showProgress();
        GetNetworkCallInteractor interactor = new OrderListInteractorImpl(mExecutor, mMainThread, this,
                new OrderListItemDataImpl());
        if (Utilities.getConnectionStatusAsInt(context) != 0)
            interactor.execute();
        else {
            view.hideProgress();
            view.showError(context.getString(R.string.error_no_internet_connection));
        }
    }

    @Override
    public void onSuccess(Object obj) {
        view.hideProgress();
        if (obj instanceof OrderListResponseModel[]) {
            OrderListResponseModel[] model = (OrderListResponseModel[]) obj;
            if (model.length > 0)
                view.displayOrderList(model);
        }

    }

    @Override
    public void onFail(String json) {
        view.hideProgress();
        view.showError(context.getString(R.string.error_unexpected_occured));
    }

    @Override
    public void onFailSocket() {
       view.hideProgress();
       view.showError(context.getString(R.string.error_connection_timeout));
    }
}

package com.example.demo.mobiversitetastmurat.domain.executor;

import com.example.demo.mobiversitetastmurat.domain.interactors.base.AbstractInteractor;

/**
 * Created by Murat.
 */

public interface Executor {
    /**
     * This method should call the interactor's run method and thus start the interactor. This should be called
     * on a background thread as interactors might do lengthy operations.
     *
     * @param interactor The interactor to run.
     */
    void execute(final AbstractInteractor interactor);
}

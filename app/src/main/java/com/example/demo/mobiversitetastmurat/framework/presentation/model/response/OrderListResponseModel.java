package com.example.demo.mobiversitetastmurat.framework.presentation.model.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Random;
import java.util.UUID;

import androidx.annotation.IdRes;

/**
 * Created by Murat.
 */

@JsonIgnoreProperties(ignoreUnknown=true)
public class OrderListResponseModel {

    private long id = new Random().nextInt(1000) ;

    @JsonProperty("date")
    public String date;
    @JsonProperty("month")
    public String month;
    @JsonProperty("restaurantName")
    public String restaurantName;
    @JsonProperty("foodIngredients")
    public String foodIngredients;
    @JsonProperty("foodPrice")
    public Double foodPrice;
    @JsonProperty("state")
    public String state;
    @JsonProperty("detail")
    public Detail detail;

    public static class Detail {

        @JsonProperty("summary")
        public String summary;
        @JsonProperty("summaryPrice")
        public Double summaryPrice;

        public String getSummary() {
            return summary;
        }

        public void setSummary(String summary) {
            this.summary = summary;
        }

        public Double getSummaryPrice() {
            return summaryPrice;
        }

        public void setSummaryPrice(Double summaryPrice) {
            this.summaryPrice = summaryPrice;
        }
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public String getFoodIngredients() {
        return foodIngredients;
    }

    public void setFoodIngredients(String foodIngredients) {
        this.foodIngredients = foodIngredients;
    }

    public Double getFoodPrice() {
        return foodPrice;
    }

    public void setFoodPrice(Double foodPrice) {
        this.foodPrice = foodPrice;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Detail getDetail() {
        return detail;
    }

    public void setDetail(Detail detail) {
        this.detail = detail;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}

package com.example.demo.mobiversitetastmurat.framework.presentation.ui.activities;

import android.os.Bundle;
import android.os.Parcelable;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.demo.mobiversitetastmurat.R;
import com.example.demo.mobiversitetastmurat.domain.executor.impl.ThreadExecutor;
import com.example.demo.mobiversitetastmurat.framework.presentation.model.response.OrderListResponseModel;
import com.example.demo.mobiversitetastmurat.framework.presentation.presenters.OrderListPresenter;
import com.example.demo.mobiversitetastmurat.framework.presentation.presenters.impl.OrderListPresenterImpl;
import com.example.demo.mobiversitetastmurat.framework.presentation.ui.adapters.ExpandableOrderListAdapter;
import com.example.demo.mobiversitetastmurat.framework.threading.MainThreadImpl;
import com.example.demo.mobiversitetastmurat.framework.utils.Constants;
import com.example.demo.mobiversitetastmurat.framework.utils.SharedPreferenceHelper;
import com.example.demo.mobiversitetastmurat.framework.utils.Utilities;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.h6ah4i.android.widget.advrecyclerview.animator.GeneralItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.animator.RefactoredDefaultItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.expandable.RecyclerViewExpandableItemManager;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements OrderListPresenter.View,
        RecyclerViewExpandableItemManager.OnGroupCollapseListener, BottomNavigationView.OnNavigationItemSelectedListener,
        RecyclerViewExpandableItemManager.OnGroupExpandListener {

    private static final String SAVED_STATE_EXPANDABLE_ITEM_MANAGER = "RecyclerViewExpandableItemManager";

    private OrderListPresenter orderListPresenter;

    @BindView(R.id.orderListRv)
    RecyclerView mRecyclerView;

    @BindView(R.id.bottomNv)
    BottomNavigationView bottomNv;

    @BindView(R.id.loadingPb)
    ProgressBar loadingPb;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mWrappedAdapter;
    private RecyclerViewExpandableItemManager mRecyclerViewExpandableItemManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        final Parcelable eimSavedState = (savedInstanceState != null) ? savedInstanceState.getParcelable(SAVED_STATE_EXPANDABLE_ITEM_MANAGER) : null;
        mRecyclerViewExpandableItemManager = new RecyclerViewExpandableItemManager(eimSavedState);

        init();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        // save current state to support screen rotation, etc...
        if (mRecyclerViewExpandableItemManager != null) {
            outState.putParcelable(
                    SAVED_STATE_EXPANDABLE_ITEM_MANAGER,
                    mRecyclerViewExpandableItemManager.getSavedState());
        }
    }

    public void init() {
        orderListPresenter = new OrderListPresenterImpl(ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(), this, this);
        setUI();

    }

    public void setUI() {
        bottomNv.setOnNavigationItemSelectedListener(this);
        bottomNv.inflateMenu(R.menu.bottom_navigation_menu_items);
        toolbar.setBackgroundColor(getResources().getColor(R.color.orange_900));
        //Order liste request attığımız yer
        orderListPresenter.getOrderList();
    }

    @Override
    public void displayOrderList(OrderListResponseModel[] orderList) {

        mLayoutManager = new LinearLayoutManager(MainActivity.this);

        mRecyclerViewExpandableItemManager.setOnGroupExpandListener(this);
        mRecyclerViewExpandableItemManager.setOnGroupCollapseListener(this);

        //adapter
        final ExpandableOrderListAdapter myItemAdapter = new ExpandableOrderListAdapter(orderList, MainActivity.this);

        mWrappedAdapter = mRecyclerViewExpandableItemManager.createWrappedAdapter(myItemAdapter);       // wrap for expanding

        final GeneralItemAnimator animator = new RefactoredDefaultItemAnimator();
        animator.setSupportsChangeAnimations(false);

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mWrappedAdapter);  // requires *wrapped* adapter
        mRecyclerView.setItemAnimator(animator);
        mRecyclerView.setHasFixedSize(false);

        mRecyclerViewExpandableItemManager.attachRecyclerView(mRecyclerView);
    }

    @Override
    public void onGroupCollapse(int groupPosition, boolean fromUser, Object payload) {
    }

    @Override
    public void onGroupExpand(int groupPosition, boolean fromUser, Object payload) {
        if (fromUser) {
            adjustScrollPositionOnGroupExpanded(groupPosition);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int itemResId = menuItem.getItemId();
        switch (itemResId) {
            case R.id.action_item_1:
                Toast.makeText(MainActivity.this, "onNavigationItemSelected Item 1", Toast.LENGTH_LONG).show();
                break;
            case R.id.action_logout:
                MaterialDialog materialDialog = Utilities.showLogoutDialog(MainActivity.this);
                materialDialog.show();
                Button submit = (Button) materialDialog.findViewById(R.id.logoutApproveBtn);
                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        materialDialog.dismiss();
                        // rememberMeStatus 1 e eşitliyoruz
                        SharedPreferenceHelper.setRememberMeStatus(MainActivity.this, Constants.DONT_REMEMBER_ME);
                        finish();
                    }
                });
                break;

            default:
                break;
        }
        return true;
    }

    @Override
    public void showProgress() {
        loadingPb.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        loadingPb.setVisibility(View.GONE);
    }

    @Override
    public void showError(String message) {
        loadingPb.setVisibility(View.GONE);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void adjustScrollPositionOnGroupExpanded(int groupPosition) {
        int childItemHeight = 75;
        int topMargin = 16; // top-spacing: 16dp
        int bottomMargin = topMargin; // bottom-spacing: 16dp

        mRecyclerViewExpandableItemManager.scrollToGroup(groupPosition, childItemHeight, topMargin, bottomMargin);
    }
}

package com.example.demo.mobiversitetastmurat.framework.network.data.impl;

import android.util.Log;

import com.example.demo.mobiversitetastmurat.framework.network.EndPoints;
import com.example.demo.mobiversitetastmurat.framework.network.RestClient;
import com.example.demo.mobiversitetastmurat.framework.network.data.OrderListData;
import com.example.demo.mobiversitetastmurat.framework.presentation.model.response.OrderListResponseModel;
import com.example.demo.mobiversitetastmurat.framework.utils.Utilities;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Murat.
 */
public class OrderListItemDataImpl implements OrderListData {
    private static String TAG = "OrderListItemDataImpl";
    private int success;
    private OrderListResponseModel[] body;

    @Override
    public OrderListResponseModel[] previewOrderListResponse() {
        EndPoints endPoints = RestClient.getService(EndPoints.class);

        Call<OrderListResponseModel[]> orderListResponseModelCall = endPoints.getOrderListResponse();

        try {
            Response<OrderListResponseModel[]> responseCall = orderListResponseModelCall.execute();
            if (responseCall.isSuccessful()) {
                setSuccess(1);
                body = responseCall.body();
            } else {
                body = null;
                setSuccess(2);
                Log.e(TAG, "previewOrderListResponseService: " + Utilities.modelAsString(responseCall.errorBody()));
            }
        } catch (IOException e) {
            e.printStackTrace();
            setSuccess(3);
        }
        return body;
    }

    @Override
    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }
}

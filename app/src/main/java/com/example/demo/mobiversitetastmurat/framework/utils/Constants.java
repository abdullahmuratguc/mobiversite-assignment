package com.example.demo.mobiversitetastmurat.framework.utils;

public class Constants {

    public static final String PREFERENCE_NAME = "myPref";
    public static final String PREFERENCE_USERNAME = "username";
    public static final String PREFERENCE_PWD = "pwd";
    public static final String PREFERENCE_REMEMBER_ME_STATUS = "rememberMe";
    public static final String PREFERENCE_ERROR = "error";
    public static final int DONT_REMEMBER_ME = 1;
    public static final int REMEMBER_ME = 0;

    public static final String ORDER_ON_ROAD = "Yolda";
    public static final String ORDER_WAITING_APPROVE = "Onay Bekliyor";
    public static final String ORDER_PREPARING = "Hazırlanıyor";

}

package com.example.demo.mobiversitetastmurat.framework.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import android.widget.Button;

import com.afollestad.materialdialogs.MaterialDialog;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.example.demo.mobiversitetastmurat.R;
import com.google.android.material.textfield.TextInputLayout;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.core.content.ContextCompat;

public class Utilities {

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public static String modelAsString(Object object) {
        String value = "";
        ObjectMapper mapper = new ObjectMapper();
        try {
            value = mapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return value;
    }

    public static final int getConnectionStatusAsInt(Context ctx) {
        int con_stat = 0;
        ConnectivityManager cm = (ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        boolean connected = info != null && info.isConnectedOrConnecting();
        if (connected) {
            boolean isWifi = info.getType() == ConnectivityManager.TYPE_WIFI;
            boolean isMobile = info.getType() == ConnectivityManager.TYPE_MOBILE;
            boolean isEthernet = info.getType() == ConnectivityManager.TYPE_ETHERNET;
            if (isWifi)
                con_stat = 1;
            else if (isMobile)
                con_stat = 2;
            else if (isEthernet)
                con_stat = 3;
        } else {
            con_stat = 0;
        }
        return con_stat;
    }

    public static boolean isPasswordValid(String password) {
        boolean isValid = false;

        if (password != null && password.length() != 0 && password.equals("123hh4"))
            isValid = true;

        return isValid;
    }

    public static boolean isUsernameValid(String username) {
        boolean isValid = false;

        if (username != null && username.length() != 0 && username.equals("ankara"))
            isValid = true;

        return isValid;
    }

    public static void resetInputLayoutColor(TextInputLayout inp, Context ctx) {
        // manually resetting the background color filter of edit text
        // Clear error field
        inp.setErrorEnabled(false);
        inp.setError(null);
        if (inp.getEditText() != null) {
            if (inp.getEditText().getBackground() != null) {
                inp.getEditText()
                        .getBackground()
                        .setColorFilter(
                                ContextCompat.getColor(ctx, R.color.light_blue_800),
                                PorterDuff.Mode.SRC_IN
                        );
            }
        }
    }

    public static MaterialDialog showLogoutDialog(Context ctx) {
        final MaterialDialog materialDialog = new MaterialDialog.Builder(ctx)
                .customView(R.layout.dialog_logout, false)
                .build();
        materialDialog.setCancelable(false);
        Button cancelButton = (Button) materialDialog.findViewById(R.id.logoutCancelBtn);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                materialDialog.dismiss();
            }
        });
        return materialDialog;
    }

    public static int getFoodStatus(String status) {
        int id = 0;
        if (status.equals(Constants.ORDER_ON_ROAD))
            id = R.drawable.bg_onroad;
        else if (status.equals(Constants.ORDER_PREPARING))
            id = R.drawable.bg_preparing;
        else if (status.equals(Constants.ORDER_WAITING_APPROVE))
            id = R.drawable.bg_waiting_approve;
        else
            id = R.drawable.bg_waiting_approve;
        return id;
    }

    public static Drawable getFoodStatusCompoundDrawable(Context ctx, String status) {
        int id = 0;
        if (status.equals(Constants.ORDER_ON_ROAD))
            id = R.drawable.ic_onroad;
        else if (status.equals(Constants.ORDER_PREPARING))
            id = R.drawable.ic_preparing;
        else if (status.equals(Constants.ORDER_WAITING_APPROVE))
            id = R.drawable.ic_waiting_approve;
        else
            id = R.drawable.ic_waiting_approve;

        Drawable volumeDrawable = ContextCompat.getDrawable(ctx, id);
        Bitmap bitmap = null;
        if (volumeDrawable != null) {
            bitmap = ((BitmapDrawable) volumeDrawable).getBitmap();
            volumeDrawable = new BitmapDrawable(ctx.getResources(),
                    Bitmap.createScaledBitmap(bitmap, 25, 25, true));
        } else
            return ContextCompat.getDrawable(ctx, id);

        return volumeDrawable;
    }

    public static String getMonth(String month) {
        String strMonth = "";
        switch (month) {
            case "01":
                strMonth = "Ocak";
                break;
            case "02":
                strMonth = "Şubat";
                break;
            case "03":
                strMonth = "Mart";
                break;
            case "04":
                strMonth = "Nisan";
                break;
            case "05":
                strMonth = "Mayıs";
                break;
            case "06":
                strMonth = "Haziran";
                break;
            case "07":
                strMonth = "Temmuz";
                break;
            case "08":
                strMonth = "Ağustos";
                break;
            case "09":
                strMonth = "Eylül";
                break;
            case "10":
                strMonth = "Ekim";
                break;
            case "11":
                strMonth = "Kasım";
                break;
            case "12":
                strMonth = "Aralık";
                break;

            default:
                strMonth = "-";
                break;

        }
        return strMonth;
    }
}

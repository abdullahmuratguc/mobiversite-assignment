package com.example.demo.mobiversitetastmurat.framework.presentation.presenters;

import com.example.demo.mobiversitetastmurat.framework.presentation.model.response.OrderListResponseModel;
import com.example.demo.mobiversitetastmurat.framework.presentation.ui.BaseView;

import java.util.List;

/**
 * Created by Murat.
 */
public interface OrderListPresenter {
    interface View extends BaseView {
        void displayOrderList(OrderListResponseModel[] orderList);
    }

    void getOrderList();
}

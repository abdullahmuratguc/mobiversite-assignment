package com.example.demo.mobiversitetastmurat.domain.interactors.impl;

import com.example.demo.mobiversitetastmurat.domain.executor.Executor;
import com.example.demo.mobiversitetastmurat.domain.executor.MainThread;
import com.example.demo.mobiversitetastmurat.domain.interactors.GetNetworkCallInteractor;
import com.example.demo.mobiversitetastmurat.domain.interactors.base.AbstractInteractor;
import com.example.demo.mobiversitetastmurat.framework.network.data.OrderListData;
import com.example.demo.mobiversitetastmurat.framework.presentation.model.response.OrderListResponseModel;

/**
 * Created by Murat.
 */
public class OrderListInteractorImpl extends AbstractInteractor implements GetNetworkCallInteractor {

    private static String TAG = "OrderListInteractorImpl";

    private Callback callback;
    private OrderListData orderListData;

    public OrderListInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback callback, OrderListData orderListData) {
        super(threadExecutor, mainThread);
        this.callback = callback;
        this.orderListData = orderListData;
    }

    @Override
    public void run() {
        final OrderListResponseModel[] model = orderListData.previewOrderListResponse();
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                int success = orderListData.getSuccess();
                if (success == 1) {
                    callback.onSuccess(model);
                } else if (success == 2) {
                    callback.onFail(null);
                } else {
                    callback.onFailSocket();
                }
            }
        });
    }
}

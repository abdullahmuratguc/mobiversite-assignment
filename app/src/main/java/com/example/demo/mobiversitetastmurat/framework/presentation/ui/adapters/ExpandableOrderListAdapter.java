package com.example.demo.mobiversitetastmurat.framework.presentation.ui.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.demo.mobiversitetastmurat.R;
import com.example.demo.mobiversitetastmurat.framework.presentation.model.response.OrderListResponseModel;
import com.example.demo.mobiversitetastmurat.framework.presentation.ui.views.ExpandableItemIndicator;
import com.example.demo.mobiversitetastmurat.framework.utils.Utilities;
import com.h6ah4i.android.widget.advrecyclerview.expandable.ExpandableItemState;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemViewHolder;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Murat
 */
public class ExpandableOrderListAdapter
        extends AbstractExpandableItemAdapter<ExpandableOrderListAdapter.MyGroupViewHolder, ExpandableOrderListAdapter.MyChildViewHolder> {

    private OrderListResponseModel[] orderListResponseModelArray;
    private Context context;
    private LayoutInflater mLayoutInflater;

    public ExpandableOrderListAdapter(OrderListResponseModel[] orderListResponseModelArray, Context context) {
        this.orderListResponseModelArray = orderListResponseModelArray;
        this.context = context;
        this.mLayoutInflater = LayoutInflater.from(context);
        setHasStableIds(true);
    }

    class MyGroupViewHolder extends AbstractExpandableItemViewHolder {

        @BindView(R.id.dayTv)
        AppCompatTextView dayTv;

        @BindView(R.id.monthTv)
        AppCompatTextView monthTv;

        @BindView(R.id.orderTitleTv)
        AppCompatTextView orderTitleTv;

        @BindView(R.id.orderContentTv)
        AppCompatTextView orderContentTv;

        @BindView(R.id.orderStatusTv)
        AppCompatTextView orderStatusTv;

        @BindView(R.id.orderPriceTv)
        AppCompatTextView orderPriceTv;

        @BindView(R.id.expandViewTv)
        ExpandableItemIndicator expandViewTv;

        public MyGroupViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        public void setupView(OrderListResponseModel model) {

            dayTv.setText(model.getDate());
            monthTv.setText(Utilities.getMonth(model.getMonth()));
            orderTitleTv.setText(model.getRestaurantName());
            orderContentTv.setText(model.getFoodIngredients());

            Drawable backgroundDrawable = ContextCompat.getDrawable(context, Utilities.getFoodStatus(model.getState()));

            Drawable volumeDrawable = Utilities.getFoodStatusCompoundDrawable(context, model.getState());
            orderStatusTv.setText(model.getState());
            orderStatusTv.setBackgroundDrawable(backgroundDrawable);
            orderStatusTv.setCompoundDrawablesWithIntrinsicBounds(volumeDrawable, null, null, null);
            orderStatusTv.setCompoundDrawablePadding(8);

            String strOrderPrice = String.valueOf(model.getFoodPrice()) + context.getString(R.string.turkish_lira_suffix_text);
            orderPriceTv.setText(strOrderPrice);
            expandViewTv.setClickable(true);

            final ExpandableItemState expandState = getExpandState();

            if (expandState.isUpdated()) {
                boolean animateIndicator = expandState.hasExpandedStateChanged();
                expandViewTv.setExpandedState(expandState.isExpanded(), animateIndicator);
            }
        }
    }

    class MyChildViewHolder extends AbstractExpandableItemViewHolder {

        @BindView(R.id.detailSummaryTv)
        AppCompatTextView detailSummaryTv;

        @BindView(R.id.detailSummaryPriceTv)
        AppCompatTextView detailSummaryPriceTv;

        public MyChildViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        public void setupView(OrderListResponseModel model) {
            detailSummaryTv.setText(model.getDetail().getSummary());

            String strDetailSummaryPrice = String.valueOf(model.getDetail().getSummaryPrice()) + context.getString(R.string.turkish_lira_suffix_text);
            detailSummaryPriceTv.setText(strDetailSummaryPrice);
        }
    }

    @Override
    public int getGroupCount() {
        return orderListResponseModelArray.length;
    }

    @Override
    public int getChildCount(int groupPosition) {
        return 1;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return orderListResponseModelArray[groupPosition].getId();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @NonNull
    @Override
    public MyGroupViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        final View v = mLayoutInflater.inflate(R.layout.row_order_list, parent, false);
        return new MyGroupViewHolder(v);
    }

    @NonNull
    @Override
    public MyChildViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        final View v = mLayoutInflater.inflate(R.layout.row_order_list_detail, parent, false);
        return new MyChildViewHolder(v);
    }

    @Override
    public void onBindGroupViewHolder(@NonNull MyGroupViewHolder orderListVH, int groupPosition, int viewType) {
        OrderListResponseModel model = orderListResponseModelArray[groupPosition];
        orderListVH.setupView(model);
    }

    @Override
    public void onBindChildViewHolder(@NonNull MyChildViewHolder detailVH, int groupPosition, int childPosition, int viewType) {
        OrderListResponseModel model = orderListResponseModelArray[groupPosition];
        detailVH.setupView(model);
    }

    @Override
    public boolean onCheckCanExpandOrCollapseGroup(@NonNull MyGroupViewHolder holder, int groupPosition,
                                                   int x, int y, boolean expand) {
        return true;
    }
}

package com.example.demo.mobiversitetastmurat.domain.interactors;

import com.example.demo.mobiversitetastmurat.domain.interactors.base.Interactor;

/**
 * Created by Murat.
 */

public interface GetNetworkCallInteractor extends Interactor {
    interface Callback {
        void onSuccess(Object obj);
        void onFail(String json);
        void onFailSocket();
    }
}

package com.example.demo.mobiversitetastmurat.domain.executor;

/**
 * Created by Murat.
 */

public interface MainThread {
    /**
     * Make runnable operation run in the main thread.
     *
     * @param runnable The runnable to run.
     */
    void post(final Runnable runnable);
}

package com.example.demo.mobiversitetastmurat.framework.network.data;

import com.example.demo.mobiversitetastmurat.framework.presentation.model.response.OrderListResponseModel;

/**
 * Created by Murat.
 */
public interface OrderListData {
    int getSuccess();
    OrderListResponseModel[] previewOrderListResponse();
}

package com.example.demo.mobiversitetastmurat.framework.presentation.presenters.base;

import com.example.demo.mobiversitetastmurat.domain.executor.Executor;
import com.example.demo.mobiversitetastmurat.domain.executor.MainThread;

/**
 * Created by Murat.
 */

public abstract class AbstractPresenter {
    protected Executor mExecutor;
    protected MainThread mMainThread;

    public AbstractPresenter(Executor executor, MainThread mainThread) {
        mExecutor = executor;
        mMainThread = mainThread;
    }
}

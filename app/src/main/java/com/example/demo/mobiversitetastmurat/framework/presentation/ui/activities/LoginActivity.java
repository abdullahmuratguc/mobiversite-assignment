package com.example.demo.mobiversitetastmurat.framework.presentation.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.CompoundButton;
import android.widget.EditText;

import com.example.demo.mobiversitetastmurat.R;
import com.example.demo.mobiversitetastmurat.framework.utils.Constants;
import com.example.demo.mobiversitetastmurat.framework.utils.SharedPreferenceHelper;
import com.example.demo.mobiversitetastmurat.framework.utils.Utilities;
import com.google.android.material.textfield.TextInputLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {

    private String strUsername, strPassword;
    private int rememberMeStatus = Constants.DONT_REMEMBER_ME;

    @BindView(R.id.usernameTil)
    TextInputLayout usernameTil;

    @BindView(R.id.passwordTil)
    TextInputLayout passwordTil;

    @BindView(R.id.usernameEt)
    AppCompatEditText usernameEt;

    @BindView(R.id.passwordEt)
    AppCompatEditText passwordEt;

    //beni hatırla butonunun aktif edilip edilmediğini kontrol ettiğimiz yer
    @OnCheckedChanged(R.id.rememberMeSwitch)
    void onSwitchStateChanged(CompoundButton button, boolean checked) {
        if (checked)
            rememberMeStatus = Constants.REMEMBER_ME;
        else
            rememberMeStatus = Constants.DONT_REMEMBER_ME;
    }

    @OnClick(R.id.loginBtn)
    void loginClicked() {
        strUsername = strUsername.trim();
        strPassword = strPassword.trim();

        if (strUsername != null && strUsername.length() > 0 && Utilities.isUsernameValid(strUsername) &&
                strPassword != null && strPassword.length() > 0 && Utilities.isPasswordValid(strPassword)) {
            //RememberMeStatus 0 ise beni hatırla seçeneği aktif, 1 ise değil
            if (rememberMeStatus == Constants.REMEMBER_ME)
                SharedPreferenceHelper.saveUsernameAndPassword(LoginActivity.this, strUsername, strPassword, rememberMeStatus);

            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
        } else {
            if (strUsername == null || strUsername.length() == 0) {
                usernameTil.setErrorEnabled(true);
                usernameTil.setError(getString(R.string.empty_username_error));
            }
            if (strUsername != null && strUsername.length() > 0 && !Utilities.isUsernameValid(strUsername)) {
                usernameTil.setErrorEnabled(true);
                usernameTil.setError(getString(R.string.wrong_username_error));
            }
            if (strPassword == null || strPassword.length() == 0) {
                passwordTil.setErrorEnabled(true);
                passwordTil.setError(getString(R.string.empty_password_error));
            }
            if (strPassword != null && strPassword.length() > 0 && !Utilities.isPasswordValid(strPassword)) {
                passwordTil.setErrorEnabled(true);
                passwordTil.setError(getString(R.string.wrong_password_error));
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        init();
    }

    private void init() {
        setUI();
    }

    private void setUI() {
        usernameEt.addTextChangedListener(generalTextWather);
        usernameEt.setText("");

        passwordEt.addTextChangedListener(generalTextWather);
        passwordEt.setText("");

        if (SharedPreferenceHelper.getRememberMeStatus(LoginActivity.this) == Constants.REMEMBER_ME) {
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
        }


    }

    private TextWatcher generalTextWather = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            //Eğer yazı değişirken bu alanda bir işlem yapmak istersek burasıyla işlem yapalım
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (usernameEt.getText().hashCode() == s.hashCode())
                getAfterText(usernameEt, s);
            else if (passwordEt.getText().hashCode() == s.hashCode()) {
                getAfterText(passwordEt, s);
            }
        }
    };

    private void getAfterText(EditText editText, Editable editable) {
        switch (editText.getId()) {
            case R.id.usernameEt:
                strUsername = "";
                if (editable != null && editable.length() > 0) {
                    strUsername = editable.toString();
                    Utilities.resetInputLayoutColor(usernameTil, LoginActivity.this);
                }
                break;

            case R.id.passwordEt:
                strPassword = "";
                if (editable != null && editable.length() > 0) {
                    strPassword = editable.toString();
                    Utilities.resetInputLayoutColor(passwordTil, LoginActivity.this);
                }
                break;

            default:
                break;
        }
    }
}

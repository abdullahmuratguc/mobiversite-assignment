package com.example.demo.mobiversitetastmurat.framework.utils;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Murat.
 * Normalde Green Robot library sini tercih ederim fakat bu kadar basit bir işlem için
 * SharedPreference ın yeterli olacağını düşündüm.
 * @GreenRobot link (http://greenrobot.org/greendao/)
 */
public class SharedPreferenceHelper {

    public static void saveUsernameAndPassword(Context ctx, String username, String password, int rememberMeStatus) {
        SharedPreferences.Editor editor = ctx.getSharedPreferences(Constants.PREFERENCE_NAME, MODE_PRIVATE).edit();
        editor.putString(Constants.PREFERENCE_USERNAME, username);
        editor.putString(Constants.PREFERENCE_PWD, password);
        editor.putInt(Constants.PREFERENCE_REMEMBER_ME_STATUS, rememberMeStatus);
        editor.apply();
    }

    public static void getUsernameAndPassword(Context ctx) {
        SharedPreferences myPrefPreferences = ctx.getSharedPreferences(Constants.PREFERENCE_NAME, MODE_PRIVATE);
        myPrefPreferences.getString(Constants.PREFERENCE_USERNAME, Constants.PREFERENCE_ERROR);
        myPrefPreferences.getString(Constants.PREFERENCE_PWD, Constants.PREFERENCE_ERROR);
        myPrefPreferences.getInt(Constants.PREFERENCE_REMEMBER_ME_STATUS, 1);

    }

    public static int getRememberMeStatus(Context ctx) {
        SharedPreferences myPrefPreferences = ctx.getSharedPreferences(Constants.PREFERENCE_NAME, MODE_PRIVATE);
        return myPrefPreferences.getInt(Constants.PREFERENCE_REMEMBER_ME_STATUS, Constants.DONT_REMEMBER_ME);

    }

    public static void setRememberMeStatus(Context ctx, int rememberMeStatus) {
        SharedPreferences.Editor editor = ctx.getSharedPreferences(Constants.PREFERENCE_NAME, MODE_PRIVATE).edit();
        editor.putInt(Constants.PREFERENCE_REMEMBER_ME_STATUS, rememberMeStatus);
        editor.apply();
    }

    public static String getUsername(Context ctx) {
        SharedPreferences myPrefPreferences = ctx.getSharedPreferences(Constants.PREFERENCE_NAME, MODE_PRIVATE);
        return myPrefPreferences.getString(Constants.PREFERENCE_USERNAME, Constants.PREFERENCE_ERROR);

    }

    public static String getPassword(Context ctx) {
        SharedPreferences myPrefPreferences = ctx.getSharedPreferences(Constants.PREFERENCE_NAME, MODE_PRIVATE);
        return myPrefPreferences.getString(Constants.PREFERENCE_PWD, Constants.PREFERENCE_ERROR);

    }
}

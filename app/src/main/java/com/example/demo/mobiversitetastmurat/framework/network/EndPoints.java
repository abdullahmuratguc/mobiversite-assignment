package com.example.demo.mobiversitetastmurat.framework.network;

import com.example.demo.mobiversitetastmurat.framework.presentation.model.response.OrderListResponseModel;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Murat.
 * This interface used for Retrofit Calls
 */

public interface EndPoints {
    /**
     * Example endpoint for Retrofit
     * Use Path to initialize a string for a specific key value
     * Use Header Authorization for giving Auth2 Auto Keys to service
     * Use RequestBody to embed body to webservice call
     */

   @GET("mobiversite/restaurant.json")
   Call<OrderListResponseModel[]> getOrderListResponse();
}

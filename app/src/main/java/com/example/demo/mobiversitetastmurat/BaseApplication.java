package com.example.demo.mobiversitetastmurat;

import android.app.Application;

/**
 * Created by Murat.
 */

public class BaseApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
    }
}
